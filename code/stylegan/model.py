from keras.models import Sequential
from keras.layers import Dense, Conv2D ,MaxPooling2D,SpatialDropout2D,Input,Flatten,Conv2DTranspose,Reshape
from keras.layers.advanced_activations import LeakyReLU

class discriminator:
    def __init__(self,size=16,base=None):
        model = Sequential(name="discriminator{}x{}".format(size,size))
        model.add(
            self.fromRGBA(size)
        )
        model.add(
            self.Conv2D(size)
        )
        # model.summary()
        if(base):

            oldlayer = Sequential(name="oldlayer")
            for layer in base.layers:
                if not layer.name == "fromRGBA":
                    oldlayer.add(
                        layer
                        )
            oldlayer.trainable=False
            model.add(
                oldlayer
            )
            
        else:
            model.add(
                Flatten()
            )
            model.add(
                Dense(1,activation='sigmoid')
            )
        # model.compile(loss='binary_crossentropy', optimizer='adagrad')
        self.model=model
        
    def summary(self):
        self.model.summary()
        pass

    def model(self):
        return self.model

    def fromRGBA(self,size):
        model=Sequential(name="fromRGBA")
        model.add(
            Conv2D(
                32,
                1,
                input_shape=(size,size,4),
                padding = 'same',
                activation='sigmoid'
            )
        )
        return model
    def Conv2D(self,size):
        n_filter = 32
        kernel_size = 3
        model=Sequential(name="discrim_{}x{}".format(size,size))
        model.add(
            Conv2D(
                n_filter,
                kernel_size,
                padding = 'same'
            )
        )
        model.add(
            LeakyReLU(0.2)
            )
        model.add(
            MaxPooling2D(
                padding='same'
            )
        )
        model.add(
            SpatialDropout2D(0.25)
        )
        return model

class generator:
    def __init__(self,size=16,base=None):
        model = Sequential(name="generator{}x{}".format(size,size))
        
        if(base):
            oldlayer = Sequential(name="oldlayer")
            for layer in base.layers:
                if not layer.name == "toRGBA":
                    oldlayer.add(
                        layer
                        )
            oldlayer.trainable=False
            model.add(
                oldlayer
            )
        else:
            model.add(
                self.fromLATENT(size=9)
            )
        model.add(
            self.Conv2DTranspose(size)
        )

        model.add(
            self.toRGBA()
            )
        # model.compile(loss='binary_crossentropy', optimizer='adagrad')

        self.model=model
        
    def summary(self):
        self.model.summary()
        pass
    def fromLATENT(self,size=10):
        model = Sequential(name="fromLATENT_{}".format(size,size))
        model.add(
            Dense(
                8*8*32,
                input_shape=(size,),
                activation='relu'
                )
            )
        model.add(
            Reshape((8,8,32))
        )
        return model
    def model(self):
        return self.model

    def toRGBA(self):
        model=Sequential(name="toRGBA")
        model.add(
            Conv2DTranspose(
                4,
                3,
                padding='same',
                activation='sigmoid'
                )
        )
        return model

    def Conv2DTranspose(self,size):

        model=Sequential(name="generator_{}x{}".format(size,size))
        model.add(
            Conv2DTranspose(
                32,
                3,
                padding = 'same',
                strides=2,
                activation='relu'
            )
        )
        model.add(
            LeakyReLU(0.2)
            )
        return model
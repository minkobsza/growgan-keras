from keras.models import Sequential
from keras.callbacks import ModelCheckpoint,TensorBoard,LambdaCallback
from glob import glob
from model import model 
import imageio
import numpy as np
import os
def loaddata(path):
    dirs = glob(path+"/*.png")
    images = np.array(list(map(
                lambda dir:np.float32(imageio.imread(dir)),
                dirs
            )))/256
    images = np.float32(images)
    
    print("loading :{}, {}".format(images.shape,images.dtype))
    return images

class autoencoder:
    def __init__(self):
         
        generator = model.generator()
        encoder = model.encoder()

        keras_generator = generator.get_model()
        keras_encoder = encoder.get_model()

        self.model = Sequential(
            [
                keras_encoder,
                keras_generator
            ],
            name='autoencoder'
        )

        self.model.compile(
            'adagrad',
            loss='logcosh',
            metrics=['mean_absolute_error']
        )
        self.model.summary()

        pass
    def train(self,images):
        self.input = images
        callbacks_list = [
            ModelCheckpoint(
                "../model/pokemon/model",
                monitor='val_loss',
                verbose=1,
                save_best_only=True,
                mode='auto',
                period=1),
            TensorBoard(
                log_dir='../tensorboard',
                histogram_freq=10, 
                batch_size=32, 
                write_graph=True, 
                write_grads=True, 
                write_images=True, 
                embeddings_freq=0, 
                embeddings_layer_names=None, 
                embeddings_metadata=None, 
                embeddings_data=None, 
                update_freq='epoch'),
            LambdaCallback(
                on_epoch_end=self.saveoutput
            )
        ]
        self.model.fit(
            x=images,
            y=images,
            batch_size=16,
            epochs=10000,
            validation_split=0.1,
            shuffle=True,
            callbacks=callbacks_list,
            verbose=2
        )

        pass
    def saveoutput(self,epoch,logs):
        if epoch%100 == 0:

            output = self.model.predict(self.input)
            output[output>1] = 1
            output[output<0] = 0
            path = "../model/pokemon/output/epoch_{}".format(epoch)
            if not os.path.exists(path):
                os.makedirs(path)
            for i in range(output.shape[0]):
                # saveimage
                PNG = np.uint8(output[i]*255)
                imageio.imwrite("{}/{}.png".format(path,i),PNG)
            print("written images to :{}".format(path))

if __name__ =="__main__":
    autoenc =  autoencoder()

    images = loaddata("../dataset/pokemon")
    autoenc.train(images)


# from keras.models import Sequential
# from keras.callbacks import ModelCheckpoint,TensorBoard,LambdaCallback
from glob import glob
import imageio
import numpy as np
import os
import cv2
def loaddata(path):
    dirs = glob(path+"/*.png")
    print("loading {} images".format(len(dirs)))
    names = list(
        map(
            lambda dir: dir.split("/")[-1],
            dirs
        )
    )
    images = list(map(
                lambda dir:np.uint8(imageio.imread(dir)),
                dirs
            ))
    
    print("loaded {} images".format(len(images)))
    return images,names

def scaleSave(images,names,size=[16,32,64,128,256],path="../dataset/stylegan/pokemon/"):
    if not os.path.exists(path):
        os.makedirs(path)
    for dim in size:
        dirname = "{}x{}/".format(dim,dim)
        imgs_scale = list(
            map(
                lambda image:cv2.resize(image,(dim,dim)),
                images
            )
        )
        out_dir = path+dirname
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)


        for i,img in enumerate(imgs_scale):
            the_dir = "{}{}".format(out_dir,names[i])
            print(the_dir)
            imageio.imwrite(the_dir,img)

if __name__ == "__main__":
    images,names = loaddata("../dataset/pokemon")
    scaleSave(images,names)

    

from keras.models import Sequential
from keras.layers import Dense, Conv2D ,MaxPooling2D,SpatialDropout2D,Input,Flatten,Conv2DTranspose,Reshape
class encoder:
    def __init__(self,layers=4):
        model = Sequential(name='encoder')
        model.add(self.conv(0,input_shape=(256,256,4)))
        for i in range(layers-1):
            model.add(self.conv(i+1))
        model.add(Flatten())

        model.add(Dense(32,activation='sigmoid'))

        self.model = model
        pass

    def get_model(self):
        return self.model
    def conv(self,index,input_shape=None):
        filters = 32
        kernel_size = 5
        strides=(1, 1)
        droprate = 0.5
        model = Sequential(name="conv2D_"+str(index))

        if input_shape:
            model.add(
                Conv2D(
                    filters,
                    kernel_size,
                    strides=strides,
                    input_shape=input_shape,
                    padding = 'same',
                    activation='relu'
                    )
                )
            
        else:
            model.add(
                Conv2D(
                    filters,
                    kernel_size,
                    strides=strides,
                    padding = 'same',
                    activation='relu'
                    )
                )
        model.add(
            MaxPooling2D(
                padding='same'
            )
        )
        model.add(
                SpatialDropout2D(droprate)
            )

        return model

class generator:
    def __init__(self,layers=4):
        model = Sequential(name='generator')

        model.add(Dense(16*16*32,input_shape=(32,)))
        model.add(Reshape((16,16,32)))
        for i in range(layers-1):
            model.add(self.Conv2DTranspose(i))

        model.add(self.Conv2DTranspose(layers,size=4))
        self.model = model

        pass

    def get_model(self):
        return self.model


    def Conv2DTranspose(self,index,size=32):
        model = Sequential(name='Conv2DTranspose_'+str(index))
        model.add(
            Conv2DTranspose(
                size,
                5,
                padding='same',
                strides=2,
                activation='relu'
                )
            )
        return model

class discriminator:
    def __init__(self):
        pass
    def __call__(self):
        print("this is discriminator")
        pass
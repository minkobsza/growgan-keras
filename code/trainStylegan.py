from stylegan import model
from keras.models import Model,load_model
from keras.layers import Input
from tqdm import tqdm
from glob import glob
import numpy as np
import imageio
import os


def loaddata(path):
    dirs = glob(path+"/*.png")
    images = np.array(list(map(
                lambda dir:np.float32(imageio.imread(dir)),
                dirs
            )))/256
    images = np.float32(images)
    
    print("loading :{}, {}".format(images.shape,images.dtype))
    return images

class stylegan:
    def __init__(self):

        size = 16
        try:
            prev = load_model('../model/stylegan/'+'stylegan{}x{}.h5'.format(size,size))
            print("loaded pretrained model")
            self.generator =prev.get_layer("generator{}x{}".format(size,size))
            self.discriminator = prev.gan.get_layer("discriminator{}x{}".format(size,size))
        except:
            print("creating model")
            self.generator = model.generator(size=size,base=None).model
            self.discriminator = model.discriminator(size=size,base=None).model


        self.gan = self.create_gan(self.generator,self.discriminator)

    def grow(self,size):
        if not size == 16:
            
            self.generator = model.generator(size=size,base=self.generator).model
            self.discriminator = model.discriminator(size=size,base=self.discriminator).model
            self.gan = self.create_gan(self.generator,self.discriminator)

    def create_gan(self,generator,discriminator):

        generator.compile(loss='binary_crossentropy', optimizer='adam')
        discriminator.compile(loss='binary_crossentropy', optimizer='adam')

        discriminator.trainable=False
        gan_input = Input(shape=(9,))
        x = generator(gan_input)
        gan_output= discriminator(x)
        gan= Model(inputs=gan_input, outputs=gan_output)
        gan.summary()
        gan.compile(loss='binary_crossentropy', optimizer='adam')
        return gan
    def model(self):
        return self.gan

    def train(self,images,epoch=1,batch_size=10,save_model_every=5000,save_output_every=5000,size=16):
        epoch_length = len(images)
        for e_ith in tqdm(range(epoch)):
            
            print("epoch {}/{}".format(e_ith,epoch))
            for i in (range(0,epoch_length,batch_size)):
                batch_images = images[i:i+batch_size]
                batch_generated_images = np.array(self.generator.predict(
                    np.random.normal(0,1, (batch_images.shape[0], 9))
                    ))
                X = np.concatenate(
                    [
                        batch_images,
                        batch_generated_images
                    ]
                )
                Y = np.concatenate(
                    [
                        np.ones(batch_images.shape[0])*0.9,
                        np.ones(batch_generated_images.shape[0])*0.1
                    ]
                )

                # pretrain discriminator
                self.discriminator.trainable=True
                self.discriminator.train_on_batch(
                    X,
                    Y,
                )

                # train generator by disable discriminator in gan
                self.discriminator.trainable=False

                noise = np.random.normal(0,1, (batch_size, 9))
                force_output_real = np.ones(batch_size)

                self.gan.train_on_batch(
                    noise,
                    force_output_real,
                )

            
            noise = np.random.normal(0,1, (batch_size, 9))
            force_output_real = np.ones(batch_size)
            print("binary_crossentropy:{}".format(self.gan.evaluate(noise,force_output_real)))

            if e_ith%save_model_every==(save_model_every-1):
                self.discriminator.trainable=True
                model_dir = '../model/stylegan/'
                if not os.path.exists(model_dir):
                    os.makedirs(model_dir)
                self.gan.save( model_dir+'stylegan{}x{}_epoch{}.h5'.format(size,size,e_ith))
                self.gan.save( model_dir+'stylegan{}x{}.h5'.format(size,size))


            if e_ith%save_output_every==(save_output_every-1):

                output_dir = '../model/stylegan/output{}x{}_epoch{}'.format(size,size,e_ith)
                if not os.path.exists(output_dir):
                    os.makedirs(output_dir)


                noise = np.random.normal(0,1, (10, 9))

                output_every_epoch = self.generator.predict(noise)
                for i in range(output_every_epoch.shape[0]):
                    # saveimage
                    PNG = np.uint8(output_every_epoch[i]*255)
                    imageio.imwrite("{}/{}.png".format(output_dir,i),PNG)

if __name__=="__main__":
    gan = stylegan()
    # images = loaddata("../dataset/stylegan/pokemon/16x16")

    # # gan.train(images,size=,batch_size=10,epoch=200)


    listOfSize = [16,32,64,128,256]
    
    for size in listOfSize:
        images = loaddata("../dataset/stylegan/pokemon/{}x{}".format(size,size))
        gan.grow(size)
        gan.train(images,batch_size=50,epoch=100000,size=size)   
